<?php

use App\Company;
use App\Load;
use App\Shift;
use App\ShiftGroup;
use App\User;
use DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

// Dashboard
Breadcrumbs::for('dashboard', function (BreadcrumbsGenerator $trail) {
    $trail->push('Dashboard', route('dashboard'));
});

// User
Breadcrumbs::for('users.index', function (BreadcrumbsGenerator $trail) {
    $trail->parent('dashboard');
    $trail->push('Users', route('users.index'));
});
Breadcrumbs::for('users.create', function (BreadcrumbsGenerator $trail) {
    $trail->parent('users.index');
    $trail->push('Create User', route('users.create'));
});
Breadcrumbs::for('users.show', function (BreadcrumbsGenerator $trail, User $user) {
    $trail->parent('users.index');
    $trail->push($user->getTitle(), route('users.show', $user));
});
Breadcrumbs::for('users.edit', function (BreadcrumbsGenerator $trail, User $user) {
    $trail->parent('users.show', $user);
    $trail->push('Edit User', route('users.edit', $user));
});

// Permission
Breadcrumbs::for('permissions.index', function (BreadcrumbsGenerator $trail) {
    $trail->parent('dashboard');
    $trail->push('Permissions', route('permissions.index'));
});
Breadcrumbs::for('permissions.show', function (BreadcrumbsGenerator $trail, Permission $permission) {
    $trail->parent('permissions.index');
    $trail->push($permission->name, route('permissions.show', $permission));
});

// Role
Breadcrumbs::for('roles.index', function (BreadcrumbsGenerator $trail) {
    $trail->parent('dashboard');
    $trail->push('Roles', route('roles.index'));
});
Breadcrumbs::for('roles.create', function (BreadcrumbsGenerator $trail) {
    $trail->parent('roles.index');
    $trail->push('Create Role', route('roles.create'));
});
Breadcrumbs::for('roles.show', function (BreadcrumbsGenerator $trail, Role $role) {
    $trail->parent('roles.index');
    $trail->push($role->name, route('roles.show', $role));
});
Breadcrumbs::for('roles.edit', function (BreadcrumbsGenerator $trail, Role $role) {
    $trail->parent('roles.show', $role);
    $trail->push('Edit Role', route('roles.edit', $role));
});

// Company
Breadcrumbs::for('companies.index', function (BreadcrumbsGenerator $trail) {
    $trail->parent('dashboard');
    $trail->push('Companies', route('companies.index'));
});
Breadcrumbs::for('companies.create', function (BreadcrumbsGenerator $trail) {
    $trail->parent('companies.index');
    $trail->push('Create Company', route('companies.create'));
});
Breadcrumbs::for('companies.show', function (BreadcrumbsGenerator $trail, Company $company) {
    $trail->parent('companies.index');
    $trail->push($company->getTitle(), route('companies.show', $company));
});
Breadcrumbs::for('companies.edit', function (BreadcrumbsGenerator $trail, Company $company) {
    $trail->parent('companies.show', $company);
    $trail->push('Edit Company', route('companies.edit', $company));
});

// Shift
Breadcrumbs::for('shifts.index', function (BreadcrumbsGenerator $trail) {
    $trail->parent('dashboard');
    $trail->push('Shifts', route('shifts.index'));
});
Breadcrumbs::for('shifts.create', function (BreadcrumbsGenerator $trail) {
    $trail->parent('shifts.index');
    $trail->push('Create Shift', route('shifts.create'));
});
Breadcrumbs::for('shifts.show', function (BreadcrumbsGenerator $trail, Shift $shift) {
    $trail->parent('shifts.index');
    $trail->push($shift->getTitle(), route('shifts.show', $shift));
});
Breadcrumbs::for('shifts.edit', function (BreadcrumbsGenerator $trail, Shift $shift) {
    $trail->parent('shifts.show', $shift);
    $trail->push('Edit Shift', route('shifts.edit', $shift));
});

// Load
Breadcrumbs::for('loads.index', function (BreadcrumbsGenerator $trail) {
    $trail->parent('dashboard');
    $trail->push('Loads', route('loads.index'));
});
Breadcrumbs::for('loads.create', function (BreadcrumbsGenerator $trail) {
    $trail->parent('loads.index');
    $trail->push('Create Load', route('loads.create'));
});
Breadcrumbs::for('loads.show', function (BreadcrumbsGenerator $trail, Load $load) {
    $trail->parent('loads.index');
    $trail->push($load->getTitle(), route('loads.show', $load));
});
Breadcrumbs::for('loads.edit', function (BreadcrumbsGenerator $trail, Load $load) {
    $trail->parent('loads.show', $load);
    $trail->push('Edit Load', route('loads.edit', $load));
});

// ShiftGroup
Breadcrumbs::for('shiftGroups.index', function (BreadcrumbsGenerator $trail) {
    $trail->parent('dashboard');
    $trail->push('Shift Groups', route('shiftGroups.index'));
});
Breadcrumbs::for('shiftGroups.show', function (BreadcrumbsGenerator $trail, ShiftGroup $shiftGroup) {
    $trail->parent('shiftGroups.index');
    $trail->push($shiftGroup->getTitle(), route('shiftGroups.show', $shiftGroup));
});
Breadcrumbs::for('shiftGroups.edit', function (BreadcrumbsGenerator $trail, ShiftGroup $shiftGroup) {
    $trail->parent('shiftGroups.show', $shiftGroup);
    $trail->push('Edit Shift Group', route('shiftGroups.edit', $shiftGroup));
});
