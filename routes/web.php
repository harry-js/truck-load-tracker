<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Company;
use App\Load;
use App\Shift;
use App\ShiftGroup;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

Auth::routes(['register' => false]);

Route::get('/', function () {
    $route = Auth::check() ? 'dashboard' : 'login';

    return redirect($route);
});
Route::middleware('auth')
    ->group(function () {
        Route::get('dashboard', 'DashboardController@index')
            ->name('dashboard');

        // User
        Route::get('users', 'UserController@index')
            ->name('users.index')
            ->middleware('can:index,' . User::class);
        Route::get('users/dataTableData', 'UserController@dataTableData')
            ->name('users.dataTableData')
            ->middleware('can:index,' . User::class);
        Route::get('users/create', 'UserController@create')
            ->name('users.create')
            ->middleware('can:create,' . User::class);
        Route::post('users', 'UserController@store')
            ->name('users.store')
            ->middleware('can:create,' . User::class);
        Route::get('users/{user}', 'UserController@show')
            ->name('users.show')
            ->middleware('can:view,user');
        Route::get('users/{user}/shiftChartData', 'UserController@shiftChartData')
            ->name('users.shiftChartData')
            ->middleware('can:shiftChartData,user');
        Route::get('users/{user}/edit', 'UserController@edit')
            ->name('users.edit')
            ->middleware('can:update,user');
        Route::put('users/{user}', 'UserController@update')
            ->name('users.update')
            ->middleware('can:update,user');
        Route::delete('users/{user}', 'UserController@destroy')
            ->name('users.destroy')
            ->middleware('can:delete,user');

        // Permission
        Route::get('permissions', 'PermissionController@index')
            ->name('permissions.index')
            ->middleware('can:index,' . Permission::class);
        Route::get('permissions/dataTableData', 'PermissionController@dataTableData')
            ->name('permissions.dataTableData')
            ->middleware('can:index,' . Permission::class);
        Route::get('permissions/{permission}', 'PermissionController@show')
            ->name('permissions.show')
            ->middleware('can:view,permission');

        // Role
        Route::get('roles', 'RoleController@index')
            ->name('roles.index')
            ->middleware('can:index,' . Role::class);
        Route::get('roles/dataTableData', 'RoleController@dataTableData')
            ->name('roles.dataTableData')
            ->middleware('can:index,' . Role::class);
        Route::get('roles/create', 'RoleController@create')
            ->name('roles.create')
            ->middleware('can:create,' . Role::class);
        Route::post('roles', 'RoleController@store')
            ->name('roles.store')
            ->middleware('can:create,' . Role::class);
        Route::get('roles/{role}', 'RoleController@show')
            ->name('roles.show')
            ->middleware('can:view,role');
        Route::get('roles/{role}/edit', 'RoleController@edit')
            ->name('roles.edit')
            ->middleware('can:update,role');
        Route::put('roles/{role}', 'RoleController@update')
            ->name('roles.update')
            ->middleware('can:update,role');
        Route::delete('roles/{role}', 'RoleController@destroy')
            ->name('roles.destroy')
            ->middleware('can:delete,role');

        // Company
        Route::get('companies', 'CompanyController@index')
            ->name('companies.index')
            ->middleware('can:index,' . Company::class);
        Route::get('companies/dataTableData', 'CompanyController@dataTableData')
            ->name('companies.dataTableData')
            ->middleware('can:index,' . Company::class);
        Route::get('companies/create', 'CompanyController@create')
            ->name('companies.create')
            ->middleware('can:create,' . Company::class);
        Route::post('companies', 'CompanyController@store')
            ->name('companies.store')
            ->middleware('can:create,' . Company::class);
        Route::get('companies/{company}', 'CompanyController@show')
            ->name('companies.show')
            ->middleware('can:view,company');
        Route::get('companies/{company}/shifts/dataTableData', 'CompanyController@shiftDataTableData')
            ->name('companies.shiftDataTableData')
            ->middleware('can:shiftDataTableData,company');
        Route::get('companies/{company}/edit', 'CompanyController@edit')
            ->name('companies.edit')
            ->middleware('can:update,company');
        Route::put('companies/{company}', 'CompanyController@update')
            ->name('companies.update')
            ->middleware('can:update,company');
        Route::delete('companies/{company}', 'CompanyController@destroy')
            ->name('companies.destroy')
            ->middleware('can:delete,company');

        // Shift
        Route::get('shifts', 'ShiftController@index')
            ->name('shifts.index')
            ->middleware('can:index,' . Shift::class);
        Route::get('shifts/dataTableData', 'ShiftController@dataTableData')
            ->name('shifts.dataTableData')
            ->middleware('can:index,' . Shift::class);
        Route::get('shifts/create', 'ShiftController@create')
            ->name('shifts.create')
            ->middleware('can:create,' . Shift::class);
        Route::post('shifts', 'ShiftController@store')
            ->name('shifts.store')
            ->middleware('can:create,' . Shift::class);
        Route::get('shifts/{shift}', 'ShiftController@show')
            ->name('shifts.show')
            ->middleware('can:view,shift');
        Route::get('shifts/{shift}/loads/dataTableData', 'ShiftController@loadDataTableData')
            ->name('shifts.loadDataTableData')
            ->middleware('can:loadDataTableData,shift');
        Route::get('shifts/{shift}/edit', 'ShiftController@edit')
            ->name('shifts.edit')
            ->middleware('can:update,shift');
        Route::put('shifts/{shift}', 'ShiftController@update')
            ->name('shifts.update')
            ->middleware('can:update,shift');
        Route::delete('shifts/{shift}', 'ShiftController@destroy')
            ->name('shifts.destroy')
            ->middleware('can:delete,shift');

        // ShiftGroup
        Route::get('shiftGroups', 'ShiftGroupController@index')
            ->name('shiftGroups.index')
            ->middleware('can:index,' . ShiftGroup::class);
        Route::get('shiftGroups/dataTableData', 'ShiftGroupController@dataTableData')
            ->name('shiftGroups.dataTableData')
            ->middleware('can:index,' . ShiftGroup::class);
        Route::get('shiftGroups/{shiftGroup}', 'ShiftGroupController@show')
            ->name('shiftGroups.show')
            ->middleware('can:view,shiftGroup');
        Route::get('shiftGroups/{shiftGroup}/shifts/dataTableData', 'ShiftGroupController@shiftDataTableData')
            ->name('shiftGroups.shiftDataTableData')
            ->middleware('can:shiftDataTableData,shiftGroup');
        Route::get('shiftGroups/{shiftGroup}/edit', 'ShiftGroupController@edit')
            ->name('shiftGroups.edit')
            ->middleware('can:update,shiftGroup');
        Route::put('shiftGroups/{shiftGroup}', 'ShiftGroupController@update')
            ->name('shiftGroups.update')
            ->middleware('can:update,shiftGroup');

        // Load
        Route::get('loads', 'LoadController@index')
            ->name('loads.index')
            ->middleware('can:index,' . Load::class);
        Route::get('loads/dataTableData', 'LoadController@dataTableData')
            ->name('loads.dataTableData')
            ->middleware('can:index,' . Load::class);
        Route::get('loads/create', 'LoadController@create')
            ->name('loads.create')
            ->middleware('can:create,' . Load::class);
        Route::post('loads', 'LoadController@store')
            ->name('loads.store')
            ->middleware('can:create,' . Load::class);
        Route::get('loads/{load}', 'LoadController@show')
            ->name('loads.show')
            ->middleware('can:view,load');
        Route::get('loads/{load}/edit', 'LoadController@edit')
            ->name('loads.edit')
            ->middleware('can:update,load');
        Route::put('loads/{load}', 'LoadController@update')
            ->name('loads.update')
            ->middleware('can:update,load');
        Route::delete('loads/{load}', 'LoadController@destroy')
            ->name('loads.destroy')
            ->middleware('can:delete,load');
    });
