@php
    /** @var \App\Load $load */
@endphp
@extends('layouts.master')
@section('head.title', 'Load Information')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('loads.show', $load))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="{{ \App\Load::ICON }}"></i> Load Information
        </div>
        <div class="card-body">
            <dl>
                <dt>Shift</dt>
                <dd>{!! $load->shift->getTitle(true) !!}</dd>
                @if($load->bobtail())
                    <dt>Bobtail</dt>
                    <dd>{!! $load->bobtailFormatted !!}</dd>
                @else
                    <dt>Trailer</dt>
                    <dd>{{ $load->trailer }}</dd>
                @endif
                <dt>From</dt>
                <dd>{{ $load->from }}</dd>
                <dt>To</dt>
                <dd>{{ $load->to }}</dd>
                <dt>Pro</dt>
                <dd>{{ $load->pro }}</dd>
                <dt>Created</dt>
                <dd>{{ $load->created_at->toDayDateTimeString() }}</dd>
                <dt>Last Updated</dt>
                <dd>{{ $load->updated_at->toDayDateTimeString() }}</dd>
            </dl>
        </div>
        @can('update', $load)
            <div class="card-footer">
                <a href="{{ route('loads.edit', $load) }}" class="btn btn-primary float-right">
                    <i class="fas fa-edit"></i> Edit Load
                </a>
            </div>
        @endcan
    </div>
@endsection
