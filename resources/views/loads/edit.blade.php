@extends('layouts.master')
@section('head.title', 'Edit Load')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('loads.edit', $load))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-edit"></i> Edit Load
        </div>
        {!! Form::model($load, ['route' => ['loads.update', $load], 'method' => 'put']) !!}
            <div class="card-body">
                @include('loads.partials.form', ['action' => 'update'])
            </div>
            <div class="card-footer">
                <div class="text-right">
                <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection
