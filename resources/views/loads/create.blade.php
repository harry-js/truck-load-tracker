@extends('layouts.master')
@section('head.title', 'Create Load')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('loads.create'))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="{{ \App\Load::ICON }}"></i> Create Load
        </div>
        {!! Form::model(new \App\Load(), ['route' => ['loads.store']]) !!}
            <div class="card-body">
                @include('loads.partials.form', ['action' => 'store'])
            </div>
            <div class="card-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection
