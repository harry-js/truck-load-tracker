<table class="table table-bordered table-hover" id="loads-table" style="width: 100% !important;">
    <thead>
    <tr>
        <th>Shift</th>
        <th>Trailer</th>
        <th>From</th>
        <th>To</th>
        <th>Pro</th>
        <th>Created</th>
        <th>Last Updated</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
    <tr>
        <td></td>
        <td><input class="form-control form-control-sm" type="number"></td>
        <td><input class="form-control form-control-sm"></td>
        <td><input class="form-control form-control-sm"></td>
        <td><input class="form-control form-control-sm" type="number"></td>
        <td><input class="form-control form-control-sm" type="date"></td>
        <td><input class="form-control form-control-sm" type="date"></td>
        <td></td>
    </tr>
    </tfoot>
</table>

@push('body.scripts')
    <script>
        app.fn.dataTable('table#loads-table', {
            ajax: "{{ route('loads.dataTableData') }}",
            columns: [
                { data: 'shift_title', name: 'shift_title', searchable: false, sortable: false },
                { data: 'trailer' },
                { data: 'from' },
                { data: 'to' },
                { data: 'pro' },
                { data: 'created_at' },
                { data: 'updated_at' },
                { data: 'actions', searchable: false, sortable: false },
            ],
            order: [[5, 'desc']],
        });
    </script>
@endpush
