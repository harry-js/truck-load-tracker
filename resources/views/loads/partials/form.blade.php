@php
/**
 * @var \App\Load $load
 */
@endphp

{{ csrf_field() }}
@if($action === 'store')
    <div class="form-group">
        {!! Form::label('shift_id', 'Shift') !!}
        {!! Form::select('shift_id', $shifts, request()->get('shift_id'), ['class' => 'form-control' . (($errors->has('shift_id'))? ' is-invalid' : ''), 'required' => true]) !!}
        @if($errors->has('shift_id'))
            <span class="form-text invalid-feedback">{{ $errors->first('shift_id') }}</span>
        @endif
    </div>
@else
    <div class="form-group">
        {!! Form::label('', 'Shift') !!}
        <br>
        {!! $load->shift->getTitle(true) !!}
    </div>
@endif
<div class="form-group">
    {!! Form::label('bobtail', 'Bobtail?', ['style' => 'margin: 0']) !!}
    <input type="hidden" name="bobtail" value="0">
    {!! Form::checkbox('bobtail', 1) !!}
    @if($errors->has('bobtail'))
        <span class="form-text invalid-feedback">{{ $errors->first('bobtail') }}</span>
    @endif
</div>
<div class="form-group" id="trailer-input-form-group">
    {!! Form::label('trailer', 'Trailer') !!}
    {!! Form::number('trailer', null, ['class' => 'form-control' . (($errors->has('trailer'))? ' is-invalid' : '')]) !!}
    @if($errors->has('trailer'))
        <span class="form-text invalid-feedback">{{ $errors->first('trailer') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('from', 'From') !!}
    {!! Form::text('from', null, ['class' => 'form-control' . (($errors->has('from'))? ' is-invalid' : ''), 'required' => true]) !!}
    @if($errors->has('from'))
        <span class="form-text invalid-feedback">{{ $errors->first('from') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('to', 'To') !!}
    {!! Form::text('to', null, ['class' => 'form-control' . (($errors->has('to'))? ' is-invalid' : ''), 'required' => true]) !!}
    @if($errors->has('to'))
        <span class="form-text invalid-feedback">{{ $errors->first('to') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('pro', 'Pro') !!}
    {!! Form::number('pro', null, ['class' => 'form-control' . (($errors->has('pro'))? ' is-invalid' : ''), 'required' => true]) !!}
    @if($errors->has('pro'))
        <span class="form-text invalid-feedback">{{ $errors->first('pro') }}</span>
    @endif
</div>

@push('body.scripts')
    <script>
        $('input#bobtail')
            .on('change', function () {
                let $trailerInputFormGroup =  $('#trailer-input-form-group');

                if ($(this).is(':checked')) {
                    $trailerInputFormGroup.hide();
                } else {
                    $trailerInputFormGroup.show();
                }
            })
            .trigger('change');
    </script>
@endpush
