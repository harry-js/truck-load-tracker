<div class="text-center btn-group btn-group-sm">
    @can('view', $load)
        <a href="{{ route('loads.show', $load) }}" class="btn btn-light" title="View load">
            <i class="fas fa-eye"></i>
        </a>
    @endcan
    @can('update', $load)
        <a href="{{ route('loads.edit', $load) }}" class="btn btn-primary" title="Edit load">
            <i class="fas fa-edit"></i>
        </a>
    @endcan
    @can('delete', $load)
        <a href="javascript:void(0)" class="btn btn-danger btn-delete" title="Delete load" data-route="{{ route('loads.destroy', $load) }}" data-confirmation-text="Are you sure you want to delete the load?">
            <i class="fas fa-trash-alt"></i>
        </a>
    @endcan
</div>
