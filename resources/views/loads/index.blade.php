@extends('layouts.master')
@section('head.title', 'Loads')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('loads.index'))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="{{ \App\Load::ICON }}"></i> Loads
        </div>
        <div class="card-body">
            @include('loads.partials.table')
        </div>
        @can('create', \App\Load::class)
            <div class="card-footer">
                <a href="{{ route('loads.create') }}" class="btn btn-primary float-right">
                    <i class="fas fa-plus"></i> Create Load
                </a>
            </div>
        @endcan
    </div>
@endsection
