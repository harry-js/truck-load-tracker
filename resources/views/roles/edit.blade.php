@extends('layouts.master')
@section('head.title', 'Edit Role')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('roles.edit', $role))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-edit"></i> Edit Role
        </div>
        {!! Form::model($role, ['route' => ['roles.update', $role], 'method' => 'put']) !!}
            <div class="card-body">
                @include('roles.partials.form', ['action' => 'update'])
            </div>
            <div class="card-footer">
                <div class="text-right">
                <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection
