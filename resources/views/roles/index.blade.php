@extends('layouts.master')
@section('head.title', 'Roles')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('roles.index'))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-gavel"></i> Roles
        </div>
        <div class="card-body">
            @include('roles.partials.table')
        </div>
        @can('create', \Spatie\Permission\Models\Role::class)
            <div class="card-footer">
                <a href="{{ route('roles.create') }}" class="btn btn-primary float-right">
                    <i class="fas fa-plus"></i> Create Role
                </a>
            </div>
        @endcan
    </div>
@endsection
