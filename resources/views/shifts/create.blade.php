@extends('layouts.master')
@section('head.title', 'Create Shift')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('shifts.create'))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-building"></i> Create Shift
        </div>
        {!! Form::model(new \App\Shift(), ['route' => ['shifts.store']]) !!}
            <div class="card-body">
                @include('shifts.partials.form', ['action' => 'store'])
            </div>
            <div class="card-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection
