@extends('layouts.master')
@section('head.title', 'Shifts')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('shifts.index'))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="{{ \App\Shift::ICON }}"></i> Shifts
        </div>
        <div class="card-body">
            @include('shifts.partials.table')
        </div>
        @can('create', \App\Shift::class)
            <div class="card-footer">
                <a href="{{ route('shifts.create') }}" class="btn btn-primary float-right">
                    <i class="fas fa-plus"></i> Create Shift
                </a>
            </div>
        @endcan
    </div>
@endsection
