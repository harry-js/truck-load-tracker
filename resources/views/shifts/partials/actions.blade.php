<div class="text-center btn-group btn-group-sm">
    @can('view', $shift)
        <a href="{{ route('shifts.show', $shift) }}" class="btn btn-light" title="View shift">
            <i class="fas fa-eye"></i>
        </a>
    @endcan
    @can('update', $shift)
        <a href="{{ route('shifts.edit', $shift) }}" class="btn btn-primary" title="Edit shift">
            <i class="fas fa-edit"></i>
        </a>
    @endcan
    @can('delete', $shift)
        <a href="javascript:void(0)" class="btn btn-danger btn-delete" title="Delete shift" data-route="{{ route('shifts.destroy', $shift) }}" data-confirmation-text="Are you sure you want to delete the shift?">
            <i class="fas fa-trash-alt"></i>
        </a>
    @endcan
</div>
