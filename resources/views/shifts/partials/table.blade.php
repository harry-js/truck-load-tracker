<table class="table table-bordered table-hover" id="shifts-table" style="width: 100% !important;">
    <thead>
    <tr>
        <th>Company</th>
        <th>User</th>
        <th>Date</th>
        <th>Total Paid</th>
        <th>Pay Status</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
    <tr>
        <td><input class="form-control form-control-sm"></td>
        <td><input class="form-control form-control-sm"></td>
        <td><input class="form-control form-control-sm" type="date"></td>
        <td><input class="form-control form-control-sm" type="number"></td>
        <td>
            <select class="form-control form-control-sm">
                <option></option>
                <option value="1">Paid</option>
                <option value="0">Unpaid</option>
            </select>
        </td>
        <td></td>
    </tr>
    </tfoot>
</table>

@push('body.scripts')
    <script>
        app.fn.dataTable('table#shifts-table', {
            ajax: "{{ route('shifts.dataTableData') }}",
            columns: [
                { data: 'company_name', name: 'group.company.name' },
                { data: 'user_name', name: 'group.user.name' },
                { data: 'date' },
                { data: 'total_paid' },
                { data: 'pay_status', name: 'group.paid' },
                { data: 'actions', searchable: false, sortable: false },
            ],
            order: [[2, 'desc']],
        });
    </script>
@endpush
