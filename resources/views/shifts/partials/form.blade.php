@php
/**
 * @var \App\Shift $shift
 */
@endphp

{{ csrf_field() }}
@if($action === 'store')
    <div class="form-group">
        {!! Form::label('company_id', 'Company') !!}
        {!! Form::select('company_id', [null => ''] + \App\Company::query()->pluck('name', 'id')->toArray(), request()->get('company_id'), ['class' => 'form-control' . (($errors->has('company_id'))? ' is-invalid' : ''), 'required' => true]) !!}
        @if($errors->has('company_id'))
            <span class="form-text invalid-feedback">{{ $errors->first('company_id') }}</span>
        @endif
    </div>
    <div class="form-group">
        {!! Form::label('user_id', 'User') !!}
        {!! Form::select('user_id', [null => ''] + \App\User::query()->pluck('name', 'id')->toArray(), null, ['class' => 'form-control' . (($errors->has('user_id'))? ' is-invalid' : ''), 'required' => true]) !!}
        @if($errors->has('user_id'))
            <span class="form-text invalid-feedback">{{ $errors->first('user_id') }}</span>
        @endif
    </div>
@else
    <div class="form-group">
        {!! Form::label('', 'Company') !!}
        <br>
        {!! $shift->group->company->getTitle(true) !!}
    </div>
    <div class="form-group">
        {!! Form::label('', 'User') !!}
        <br>
        {!! $shift->group->user->getTitle(true) !!}
    </div>
@endif
<div class="form-group">
    {!! Form::label('date', 'Date') !!}
    {!! Form::date('date', (($action === 'update') ? $shift->date->toDateString() : null), ['class' => 'form-control' . (($errors->has('date'))? ' is-invalid' : ''), 'required' => true]) !!}
    @if($errors->has('date'))
        <span class="form-text invalid-feedback">{{ $errors->first('date') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('truck', 'Truck') !!}
    {!! Form::number('truck', null, ['class' => 'form-control' . (($errors->has('truck'))? ' is-invalid' : ''), 'required' => true]) !!}
    @if($errors->has('truck'))
        <span class="form-text invalid-feedback">{{ $errors->first('truck') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('pay', 'Pay') !!}
    {!! Form::number('pay', null, ['class' => 'form-control' . (($errors->has('pay'))? ' is-invalid' : ''), 'step' => 0.01]) !!}
    @if($errors->has('pay'))
        <span class="form-text invalid-feedback">{{ $errors->first('pay') }}</span>
    @endif
</div>
