@php
/**
 * @var \App\Shift $shift
 */
@endphp

<dl>
    <dt>Group</dt>
    <dd>{!! $shift->group->getTitle(true) !!}</dd>
    <dt>Company</dt>
    <dd>{!! $shift->group->company->getTitle(true) !!}</dd>
    <dt>User</dt>
    <dd>{!! $shift->group->user->getTitle(true) !!}</dd>
    <dt>Date</dt>
    <dd>{{ $shift->date_formatted }}</dd>
    <dt>Truck</dt>
    <dd>{{ $shift->truck }}</dd>
    @if($shift->total_paid_formatted)
        <dt>Total Paid</dt>
        <dd>{!! $shift->total_paid_formatted !!}</dd>
    @endif
    <dt>Pay Status</dt>
    <dd>{!! $shift->group->pay_status !!}</dd>
    <dt>Created</dt>
    <dd>{{ $shift->created_at->toDayDateTimeString() }}</dd>
    <dt>Last Updated</dt>
    <dd>{{ $shift->updated_at->toDayDateTimeString() }}</dd>
</dl>
