<table class="table table-bordered table-hover" id="loads-table" style="width: 100% !important;">
    <thead>
        <tr>
            <th>Trailer</th>
            <th>From</th>
            <th>To</th>
            <th>Pro</th>
            <th>Created</th>
            <th>Last Updated</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
        <tr>
            <td><input class="form-control form-control-sm" type="number"></td>
            <td><input class="form-control form-control-sm"></td>
            <td><input class="form-control form-control-sm"></td>
            <td><input class="form-control form-control-sm" type="number"></td>
            <td><input class="form-control form-control-sm" type="date"></td>
            <td><input class="form-control form-control-sm" type="date"></td>
            <td></td>
        </tr>
    </tfoot>
</table>

@push('body.scripts')
    <script>
        app.fn.dataTable('table#loads-table', {
            ajax: "{{ route('shifts.loadDataTableData', $shift) }}",
            columns: [
                { data: 'trailer' },
                { data: 'from' },
                { data: 'to' },
                { data: 'pro' },
                { data: 'created_at' },
                { data: 'updated_at' },
                { data: 'actions', searchable: false, sortable: false },
            ],
            order: [[4, 'desc']],
        });
    </script>
@endpush
