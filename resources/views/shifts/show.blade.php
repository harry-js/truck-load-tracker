@php
    /** @var \App\Shift $shift */
@endphp
@extends('layouts.master')
@section('head.title', 'Shift Information')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('shifts.show', $shift))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="{{ \App\Shift::ICON }}"></i> Shift Information
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="details-tab" data-toggle="tab" href="#details" role="tab" aria-controls="details" aria-selected="true">Details</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="loads-tab" data-toggle="tab" href="#loads" role="tab" aria-controls="loads" aria-selected="false">Loads</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="details-tab">
                    <br>
                    @include('shifts.partials.show.tabs.details')
                </div>
                <div class="tab-pane fade" id="loads" role="tabpanel" aria-labelledby="loads-tab">
                    <br>
                    @include('shifts.partials.show.tabs.loads')
                </div>
            </div>
        </div>
        <div class="card-footer">
            @can('create', \App\Load::class)
                <a href="{{ route('loads.create') . "?shift_id={$shift->id}" }}" class="btn btn-outline-primary">
                    <i class="fas fa-plus"></i> Create Load
                </a>
            @endcan
            @can('update', $shift)
                <a href="{{ route('shifts.edit', $shift) }}" class="btn btn-primary float-right">
                    <i class="fas fa-edit"></i> Edit Shift
                </a>
            @endcan
        </div>
    </div>
@endsection
