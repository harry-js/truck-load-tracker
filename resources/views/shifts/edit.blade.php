@extends('layouts.master')
@section('head.title', 'Edit Shift')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('shifts.edit', $shift))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-edit"></i> Edit Shift
        </div>
        {!! Form::model($shift, ['route' => ['shifts.update', $shift], 'method' => 'put']) !!}
            <div class="card-body">
                @include('shifts.partials.form', ['action' => 'update'])
            </div>
            <div class="card-footer">
                <div class="text-right">
                <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection
