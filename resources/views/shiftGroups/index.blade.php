@extends('layouts.master')
@section('head.title', 'ShiftGroups')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('shiftGroups.index'))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="{{ \App\ShiftGroup::ICON }}"></i> Shift Groups
        </div>
        <div class="card-body">
            @include('shiftGroups.partials.table')
        </div>
    </div>
@endsection
