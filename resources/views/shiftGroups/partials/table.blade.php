<table class="table table-bordered table-hover" id="shift-groups-table" style="width: 100% !important;">
    <thead>
    <tr>
        <th>#</th>
        <th>Company</th>
        <th>User</th>
        <th>First Shift Date</th>
        <th>Last Shift Date</th>
        <th>Pay Status</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
    <tr>
        <td><input class="form-control form-control-sm" type="number"></td>
        <td><input class="form-control form-control-sm"></td>
        <td><input class="form-control form-control-sm"></td>
        <td></td>
        <td></td>
        <td>
            <select class="form-control form-control-sm">
                <option></option>
                <option value="1">Paid</option>
                <option value="0">Unpaid</option>
            </select>
        </td>
        <td></td>
    </tr>
    </tfoot>
</table>

@push('body.scripts')
    <script>
        app.fn.dataTable('table#shift-groups-table', {
            ajax: "{{ route('shiftGroups.dataTableData') }}",
            columns: [
                { data: 'id', name: 'id', width: 50 },
                { data: 'company_name', name: 'company.name' },
                { data: 'user_name', name: 'user.name' },
                { data: 'first_shift_date', name: 'first_shift_date', searchable: false, sortable: false },
                { data: 'last_shift_date', name: 'last_shift_date', searchable: false, sortable: false },
                { data: 'pay_status', name: 'paid' },
                { data: 'actions', searchable: false, sortable: false },
            ],
            order: [[0, 'desc']],
        });
    </script>
@endpush
