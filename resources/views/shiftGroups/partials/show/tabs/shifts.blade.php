<table class="table table-bordered table-hover" id="shifts-table" style="width: 100% !important;">
    <thead>
        <tr>
            <th>Date</th>
            <th>Truck</th>
            <th>Total Paid</th>
            <th>Created</th>
            <th>Last Updated</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
        <tr>
            <td><input class="form-control form-control-sm" type="date"></td>
            <td><input class="form-control form-control-sm" type="number"></td>
            <td><input class="form-control form-control-sm" type="number"></td>
            <td><input class="form-control form-control-sm" type="date"></td>
            <td><input class="form-control form-control-sm" type="date"></td>
            <td></td>
        </tr>
    </tfoot>
</table>

@push('body.scripts')
    <script>
        app.fn.dataTable('table#shifts-table', {
            ajax: "{{ route('shiftGroups.shiftDataTableData', $shiftGroup) }}",
            columns: [
                { data: 'date' },
                { data: 'truck' },
                { data: 'total_paid' },
                { data: 'created_at' },
                { data: 'updated_at' },
                { data: 'actions', searchable: false, sortable: false },
            ]
        });
    </script>
@endpush
