@php
/**
 * @var \App\ShiftGroup $shiftGroup
 */
@endphp

<dl>
    <dt>Company</dt>
    <dd>{!! $shiftGroup->company->getTitle(true) !!}</dd>
    <dt>User</dt>
    <dd>{!! $shiftGroup->user->getTitle(true) !!}</dd>
    <dt>Pay Status</dt>
    <dd>{!! $shiftGroup->pay_status !!}</dd>
    @if($shiftGroup->isPaid())
        @if($shiftGroup->paid_on_formatted)
            <dt>Paid On</dt>
            <dd>{{ $shiftGroup->paid_on_formatted }}</dd>
        @endif
        <dt>Total Paid</dt>
        <dd>{!! $shiftGroup->total_paid !!}</dd>
    @endif
    <dt>First Shift Date</dt>
    <dd>{{ $shiftGroup->first_shift_date }}</dd>
    <dt>Last Shift Date</dt>
    <dd>{{ $shiftGroup->last_shift_date }}</dd>
</dl>
