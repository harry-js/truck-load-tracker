<div class="text-center btn-group btn-group-sm">
    @can('view', $shiftGroup)
        <a href="{{ route('shiftGroups.show', $shiftGroup) }}" class="btn btn-light" title="View Shift Group">
            <i class="fas fa-eye"></i>
        </a>
    @endcan
    @can('update', $shiftGroup)
        <a href="{{ route('shiftGroups.edit', $shiftGroup) }}" class="btn btn-primary" title="Edit Shift Group">
            <i class="fas fa-edit"></i>
        </a>
    @endcan
</div>
