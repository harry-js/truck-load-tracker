@php
    /** @var \App\ShiftGroup $shiftGroup */
@endphp
@extends('layouts.master')
@section('head.title', 'ShiftGroup Information')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('shiftGroups.show', $shiftGroup))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="{{ \App\ShiftGroup::ICON }}"></i> Shift Group Information
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="details-tab" data-toggle="tab" href="#details" role="tab" aria-controls="details" aria-selected="true">Details</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="shifts-tab" data-toggle="tab" href="#shifts" role="tab" aria-controls="shifts" aria-selected="false">Shifts</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="details-tab">
                    <br>
                    @include('shiftGroups.partials.show.tabs.details')
                </div>
                <div class="tab-pane fade" id="shifts" role="tabpanel" aria-labelledby="shifts-tab">
                    <br>
                    @include('shiftGroups.partials.show.tabs.shifts')
                </div>
            </div>
        </div>
        <div class="card-footer">
            @can('update', $shiftGroup)
                <a href="{{ route('shiftGroups.edit', $shiftGroup) }}" class="btn btn-primary float-right">
                    <i class="fas fa-edit"></i> Edit Shift Group
                </a>
            @endcan
        </div>
    </div>
@endsection
