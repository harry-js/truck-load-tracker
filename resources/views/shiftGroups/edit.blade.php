@php
/**
 * @var \App\ShiftGroup $shiftGroup
 */
@endphp

@extends('layouts.master')
@section('head.title', 'Edit Shift Group')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('shiftGroups.edit', $shiftGroup))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-edit"></i> Edit Shift Group
        </div>
        {!! Form::model($shiftGroup, ['route' => ['shiftGroups.update', $shiftGroup], 'method' => 'put']) !!}
            <div class="card-body">
                {{ csrf_field() }}
                <div class="form-group">
                    {!! Form::label('paid', 'Paid?') !!}
                    {!! Form::checkbox('paid', 1, ($shiftGroup->isPaid() ? true : false)) !!}
                    @if($errors->has('paid'))
                        <span class="form-text invalid-feedback">{{ $errors->first('paid') }}</span>
                    @endif
                </div>
                <div class="form-group" id="paid-on-form-group" style="display: none">
                    {!! Form::label('paid_on', 'Paid on') !!}
                    {!! Form::date('paid_on', (($shiftGroup->isPaid() && $shiftGroup->paid_on) ? $shiftGroup->paid_on->toDateString() : null), ['class' => 'form-control' . (($errors->has('paid_on'))? ' is-invalid' : '')]) !!}
                    @if($errors->has('paid_on'))
                        <span class="form-text invalid-feedback">{{ $errors->first('paid_on') }}</span>
                    @endif
                </div>
            </div>
            <div class="card-footer">
                <div class="text-right">
                <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection

@push('body.scripts')
    <script>
        $(function () {
            let $paid = $('input[name=paid]');

            $paid
                .on('change', function () {
                    let $paidOnFormGroup = $('#paid-on-form-group');

                    if ($paid.prop('checked')) {
                        $paidOnFormGroup.show();
                    } else {
                        $paidOnFormGroup.hide();
                    }
                })
                .trigger('change');
        });
    </script>
@endpush
