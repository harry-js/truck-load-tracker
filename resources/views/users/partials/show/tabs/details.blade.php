<dl>
    <dt>Name</dt>
    <dd>{{ $user->name }}</dd>
    <dt>Email</dt>
    <dd><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></dd>
    @if(!empty($yearlyTotalPaid))
        <dt>Yearly Payment</dt>
        <dd>
            <ul class="list-unstyled">
                @foreach($yearlyTotalPaid as $year => $totalPaid)
                    <li>{!! "{$year}: {$totalPaid}" !!}</li>
                @endforeach
            </ul>
        </dd>
    @endif
    <dt>Created</dt>
    <dd>{{ $user->created_at->toDayDateTimeString() }}</dd>
    <dt>Last Updated</dt>
    <dd>{{ $user->updated_at->toDayDateTimeString() }}</dd>
</dl>
