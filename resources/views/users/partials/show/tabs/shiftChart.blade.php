<form class="form-inline" id="generate-shift-chart-form">
    <div class="form-group mx-sm-3 mb-2">
        <label for="year" class="sr-only">Year</label>
        <input type="text" class="form-control" id="year" placeholder="Year" required>
    </div>
    <button type="submit" class="btn btn-primary mb-2">Generate</button>
</form>
<div id="chart"></div>

@push('body.scripts')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load("current", {packages:['corechart']});

        function drawChart(chartData) {
            chartData = Array.prototype.concat(
                [
                    ["Month", "Shifts"],
                ],
                chartData
            );

            var data = google.visualization.arrayToDataTable(chartData);
            var view = new google.visualization.DataView(data);
            view.setColumns([
                0,
                1,
                {
                    calc: "stringify",
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation"
                },
            ]);

            var options = {
                title: "Shifts per month",
                bar: {groupWidth: "95%"},
                legend: { position: "none" },
            };
            var chart = new google.visualization.ColumnChart(document.getElementById("chart"));
            chart.draw(view, options);
        }

        $('form#generate-shift-chart-form').submit(function (e) {
            e.preventDefault();

            let year = $('input#year').val();

            $.ajax({
                url: '{{ route('users.shiftChartData', $user) }}',
                data: {
                    year: year,
                },
                success: function (data, textStatus, jqXHR) {
                    if (!_.isEmpty(data)) {
                        let chartData = [];

                        $.each(data, function (monthName, shifts) {
                            chartData.push([monthName, shifts]);
                        });

                        drawChart(chartData);
                    } else {
                        $('#chart').empty();
                        alert('No shift data found for year ' + year);
                    }
                }
            });
        });
    </script>
@endpush
