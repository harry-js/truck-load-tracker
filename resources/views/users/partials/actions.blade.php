<div class="text-center btn-group btn-group-sm">
    @can('view', $user)
        <a href="{{ route('users.show', $user) }}" class="btn btn-light" title="View user">
            <i class="fas fa-eye"></i>
        </a>
    @endcan
    @can('update', $user)
        <a href="{{ route('users.edit', $user) }}" class="btn btn-primary" title="Edit user">
            <i class="fas fa-edit"></i>
        </a>
    @endcan
    @can('delete', $user)
        <a href="javascript:void(0)" class="btn btn-danger btn-delete" title="Delete user" data-route="{{ route('users.destroy', $user) }}" data-confirmation-text="Are you sure you want to delete the user?">
            <i class="fas fa-trash-alt"></i>
        </a>
    @endcan
</div>
