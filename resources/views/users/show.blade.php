@php
    /** @var \App\User $user */
@endphp
@extends('layouts.master')
@section('head.title', 'User Information')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('users.show', $user))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-user"></i> User Information
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="details-tab" data-toggle="tab" href="#details" role="tab" aria-controls="details" aria-selected="true">Details</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="shift-chart-tab" data-toggle="tab" href="#shift-chart" role="tab" aria-controls="shift-chart" aria-selected="true">Shift Chart</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="details-tab">
                    <br>
                    @include('users.partials.show.tabs.details')
                </div>
                <div class="tab-pane fade" id="shift-chart" role="tabpanel" aria-labelledby="shift-chart-tab">
                    <br>
                    @include('users.partials.show.tabs.shiftChart')
                </div>
            </div>
        </div>
        @can('update', $user)
            <div class="card-footer">
                <a href="{{ route('users.edit', $user) }}" class="btn btn-primary float-right">
                    <i class="fas fa-edit"></i> Edit User
                </a>
            </div>
        @endcan
    </div>
@endsection
