@extends('layouts.master')
@section('head.title', 'Create User')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('users.create'))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-user"></i> Create User
        </div>
        {!! Form::model(new \App\User(), ['route' => ['users.store']]) !!}
            <div class="card-body">
                @include('users.partials.form', ['action' => 'store'])
            </div>
            <div class="card-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection
