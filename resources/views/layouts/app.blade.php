<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @if (trim($__env->yieldContent('head.title')))
            @yield('head.title') |
        @endif
            {{ config('app.name', 'App') }}
    </title>

    <!-- Scripts -->
    <script>
        let app = {};
    </script>
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @yield('head.end')
</head>
<body>
    @include('layouts.partials.navigation')
    <main class="py-4">
        <div class="container">
            @yield('content')
        </div>
    </main>
</body>
</html>
