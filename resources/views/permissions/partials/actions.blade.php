<div class="text-center btn-group btn-group-sm">
    @can('view', $permission)
        <a href="{{ route('permissions.show', $permission) }}" class="btn btn-light" title="View permission">
            <i class="fas fa-eye"></i>
        </a>
    @endcan
</div>
