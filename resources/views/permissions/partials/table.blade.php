<table class="table table-bordered table-hover" id="permissions-table">
    <thead>
        <tr>
            <th>Name</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
        <tr>
            <td><input class="form-control form-control-sm"></td>
            <td></td>
        </tr>
    </tfoot>
</table>

@push('body.scripts')
    <script>
        app.fn.dataTable('table#permissions-table', {
            ajax: "{{ route('permissions.dataTableData') }}",
            columns: [
                { data: 'name' },
                { data: 'actions', searchable: false, sortable: false },
            ],
        });
    </script>
@endpush
