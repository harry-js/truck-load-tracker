@php
    /** @var \Spatie\Permission\Models\Permission $permission */
@endphp
@extends('layouts.master')
@section('head.title', 'Permission Information')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('permissions.show', $permission))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-shield-alt"></i> Permission Information
        </div>
        <div class="card-body">
            <dl>
                <dt>Name</dt>
                <dd>{{ $permission->name }}</dd>
                @if($roles = $permission->roles->implode('name', ', '))
                    <dt>Roles</dt>
                    <dd>{{ $roles }}</dd>
                @endif
            </dl>
        </div>
    </div>
@endsection
