@extends('layouts.master')
@section('head.title', 'Companies')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('companies.index'))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-building"></i> Companies
        </div>
        <div class="card-body">
            @include('companies.partials.table')
        </div>
        @can('create', \App\Company::class)
            <div class="card-footer">
                <a href="{{ route('companies.create') }}" class="btn btn-primary float-right">
                    <i class="fas fa-plus"></i> Create Company
                </a>
            </div>
        @endcan
    </div>
@endsection
