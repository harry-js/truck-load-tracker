<div class="text-center btn-group btn-group-sm">
    @can('view', $company)
        <a href="{{ route('companies.show', $company) }}" class="btn btn-light" title="View company">
            <i class="fas fa-eye"></i>
        </a>
    @endcan
    @can('update', $company)
        <a href="{{ route('companies.edit', $company) }}" class="btn btn-primary" title="Edit company">
            <i class="fas fa-edit"></i>
        </a>
    @endcan
    @can('delete', $company)
        <a href="javascript:void(0)" class="btn btn-danger btn-delete" title="Delete company" data-route="{{ route('companies.destroy', $company) }}" data-confirmation-text="Are you sure you want to delete the company?">
            <i class="fas fa-trash-alt"></i>
        </a>
    @endcan
</div>
