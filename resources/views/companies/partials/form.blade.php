{{ csrf_field() }}
<div class="form-group">
    {!! Form::label('name', 'Name') !!}
    {!! Form::text('name', null, ['class' => 'form-control' . (($errors->has('name'))? ' is-invalid' : ''), 'required' => true]) !!}
    @if($errors->has('name'))
        <span class="form-text invalid-feedback">{{ $errors->first('name') }}</span>
    @endif
</div>
