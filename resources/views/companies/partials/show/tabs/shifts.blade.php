<table class="table table-bordered table-hover" id="shifts-table" style="width: 100% !important;">
    <thead>
        <tr>
            <th>Date</th>
            <th>User</th>
            <th>Truck</th>
            <th>Total Paid</th>
            <th>Pay Status</th>
            <th>Created</th>
            <th>Last Updated</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
        <tr>
            <td></td>
            <td><input class="form-control form-control-sm"></td>
            <td><input class="form-control form-control-sm" type="number"></td>
            <td><input class="form-control form-control-sm" type="number"></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </tfoot>
</table>

@push('body.scripts')
    <script>
        app.fn.dataTable('table#shifts-table', {
            ajax: "{{ route('companies.shiftDataTableData', $company) }}",
            columns: [
                { data: 'date' },
                { data: 'user_name', name: 'group.user.name' },
                { data: 'truck' },
                { data: 'total_paid' },
                { data: 'pay_status', name: 'group.paid' },
                { data: 'created_at' },
                { data: 'updated_at' },
                { data: 'actions', searchable: false, sortable: false },
            ],
            order: [[5, 'desc']]
        });
    </script>
@endpush
