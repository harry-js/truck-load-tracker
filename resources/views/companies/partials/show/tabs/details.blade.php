<dl>
    <dt>Name</dt>
    <dd>{{ $company->name }}</dd>
    <dt>Created</dt>
    <dd>{{ $company->created_at->toDayDateTimeString() }}</dd>
    <dt>Last Updated</dt>
    <dd>{{ $company->updated_at->toDayDateTimeString() }}</dd>
</dl>
