@extends('layouts.master')
@section('head.title', 'Create Company')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('companies.create'))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-building"></i> Create Company
        </div>
        {!! Form::model(new \App\Company(), ['route' => ['companies.store']]) !!}
            <div class="card-body">
                @include('companies.partials.form', ['action' => 'store'])
            </div>
            <div class="card-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection
