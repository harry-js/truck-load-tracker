@php
    /** @var \App\Company $company */
@endphp
@extends('layouts.master')
@section('head.title', 'Company Information')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('companies.show', $company))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-building"></i> Company Information
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="details-tab" data-toggle="tab" href="#details" role="tab" aria-controls="details" aria-selected="true">Details</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="shifts-tab" data-toggle="tab" href="#shifts" role="tab" aria-controls="shifts" aria-selected="false">Shifts</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="details-tab">
                    <br>
                    @include('companies.partials.show.tabs.details')
                </div>
                <div class="tab-pane fade" id="shifts" role="tabpanel" aria-labelledby="shifts-tab">
                    <br>
                    @include('companies.partials.show.tabs.shifts')
                </div>
            </div>
        </div>
        <div class="card-footer">
            @can('create', \App\Shift::class)
                <a href="{{ route('shifts.create') . "?company_id={$company->id}" }}" class="btn btn-outline-primary">
                    <i class="fas fa-plus"></i> Create Shift
                </a>
            @endcan
            @can('update', $company)
                <a href="{{ route('companies.edit', $company) }}" class="btn btn-primary float-right">
                    <i class="fas fa-edit"></i> Edit Company
                </a>
            @endcan
        </div>
    </div>
@endsection
