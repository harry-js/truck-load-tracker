app.fn = {};

/**
 * @param tableSelector
 * @param options
 * @returns object|DataTables API object
 */
app.fn.dataTable = function (tableSelector, options = {}) {
    options = _.merge({
        processing: true,
        serverSide: true,
        responsive: true,
        initComplete: function () {
            let dataTable = this.api();

            dataTable.columns()
                .every(function() {
                    let that = this;

                    $(':input', this.footer()).on('change', function() {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });

            $('.btn-delete').on('click', function () {
                let $button = $(this);
                let confirmationText = $button.data('confirmation-text') ?
                    $button.data('confirmation-text') :
                    'Are you sure you want to perform the delete action?';

                if (confirm(confirmationText)) {
                    $.ajax({
                        type: 'DELETE',
                        url: $button.data('route'),
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content')
                        },
                        complete: function(){
                            dataTable.ajax
                                .reload();
                        }
                    });
                }
            });
        }
    }, options);

    return $(tableSelector).DataTable(options);
};

$(document).on('click', '.btn-confirm', function() {
    let $button = $(this);
    let confirmationText = $button.data('confirmation-text') ?
        $button.data('confirmation-text') :
        'Are you sure you want to perform this action?';

    if (confirm(confirmationText)) {
        let ajaxSettings = {
            type: $button.data('method') ?
                $button.data('method') :
                'GET',
            url: $button.data('url'),
            data: {
                _token: $('meta[name="csrf-token"]').attr('content')
            }
        };

        if ($button.data('refresh-datatable')) {
            ajaxSettings.success = function() {
                $($button.data('refresh-datatable'))
                    .DataTable()
                    .ajax
                    .reload();
            };
        }

        $.ajax(ajaxSettings);
    }
});
