<?php

namespace App\Contracts;

interface Title
{
    /**
     * Get title for an object.
     *
     * @return string
     */
    public function getTitle(): string;
}
