<?php

namespace App;

use App\Contracts\Title as TitleContract;
use App\Traits\Title as TitleTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
abstract class MyModel extends Model implements TitleContract
{
    use TitleTrait;
}
