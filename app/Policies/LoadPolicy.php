<?php

namespace App\Policies;

use App\Load;
use App\Entities\Permission as PermissionEntity;
use App\Policies\Traits\SuperAdminPassThru;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LoadPolicy
{
    use HandlesAuthorization;
    use SuperAdminPassThru;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->can(PermissionEntity::VIEW_LOADS);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can(PermissionEntity::CREATE_LOAD);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Load $model
     * @return mixed
     */
    public function view(User $user, Load $model)
    {
        return $user->can(PermissionEntity::VIEW_LOAD);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Load $model
     * @return mixed
     */
    public function update(User $user, Load $model)
    {
        return $user->can(PermissionEntity::EDIT_LOAD);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param Load $model
     * @return mixed
     */
    public function delete(User $user, Load $model)
    {
        return $user->can(PermissionEntity::DELETE_LOAD);
    }
}
