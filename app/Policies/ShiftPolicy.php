<?php

namespace App\Policies;

use App\Policies\Traits\SuperAdminPassThru;
use App\Shift;
use App\Entities\Permission as PermissionEntity;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ShiftPolicy
{
    use HandlesAuthorization;
    use SuperAdminPassThru;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->can(PermissionEntity::VIEW_SHIFTS);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can(PermissionEntity::CREATE_SHIFT);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Shift $model
     * @return mixed
     */
    public function view(User $user, Shift $model)
    {
        return $user->can(PermissionEntity::VIEW_SHIFT);
    }

    /**
     * @param User $user
     * @param Shift $model
     * @return mixed
     */
    public function loadDataTableData(User $user, Shift $model)
    {
        return $user->can(PermissionEntity::VIEW_SHIFT); // same as view
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Shift $model
     * @return mixed
     */
    public function update(User $user, Shift $model)
    {
        return $user->can(PermissionEntity::EDIT_SHIFT);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param Shift $model
     * @return mixed
     */
    public function delete(User $user, Shift $model)
    {
        return $user->can(PermissionEntity::DELETE_SHIFT);
    }
}
