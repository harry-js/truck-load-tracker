<?php

namespace App\Policies\Traits;

use App\User;

trait SuperAdminPassThru
{
    /**
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }
}
