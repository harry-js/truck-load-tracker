<?php

namespace App\Policies;

use App\Entities\Permission as PermissionEntity;
use App\Policies\Traits\SuperAdminPassThru;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Spatie\Permission\Models\Permission;

class PermissionPolicy
{
    use HandlesAuthorization;
    use SuperAdminPassThru;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->can(PermissionEntity::VIEW_PERMISSIONS);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Permission $model
     * @return mixed
     */
    public function view(User $user, Permission $model)
    {
        return $user->can(PermissionEntity::VIEW_PERMISSION);
    }
}
