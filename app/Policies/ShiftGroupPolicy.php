<?php

namespace App\Policies;

use App\Policies\Traits\SuperAdminPassThru;
use App\ShiftGroup;
use App\Entities\Permission as PermissionEntity;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ShiftGroupPolicy
{
    use HandlesAuthorization;
    use SuperAdminPassThru;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->can(PermissionEntity::VIEW_SHIFT_GROUPS);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param ShiftGroup $model
     * @return mixed
     */
    public function view(User $user, ShiftGroup $model)
    {
        return $user->can(PermissionEntity::VIEW_SHIFT_GROUP);
    }

    /**
     * @param User $user
     * @param ShiftGroup $model
     * @return mixed
     */
    public function shiftDataTableData(User $user, ShiftGroup $model)
    {
        return $user->can(PermissionEntity::VIEW_SHIFT_GROUP); // same as view
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param ShiftGroup $shiftGroup
     * @return mixed
     */
    public function update(User $user, ShiftGroup $shiftGroup)
    {
        return $user->can(PermissionEntity::EDIT_SHIFT_GROUP);
    }
}
