<?php

namespace App\Entities;

class Permission
{
    // User
    public const VIEW_USERS = 'User: Index';
    public const CREATE_USER = 'User: Create';
    public const VIEW_USER = 'User: Show';
    public const EDIT_USER = 'User: Edit';
    public const DELETE_USER = 'User: Delete';

    // Permission
    public const VIEW_PERMISSIONS = 'Permission: Index';
    public const VIEW_PERMISSION = 'Permission: Show';

    // Role
    public const VIEW_ROLES = 'Role: Index';
    public const CREATE_ROLE = 'Role: Create';
    public const VIEW_ROLE = 'Role: Show';
    public const EDIT_ROLE = 'Role: Edit';
    public const DELETE_ROLE = 'Role: Delete';

    // Company
    public const VIEW_COMPANIES = 'Company: Index';
    public const CREATE_COMPANY = 'Company: Create';
    public const VIEW_COMPANY = 'Company: Show';
    public const EDIT_COMPANY = 'Company: Edit';
    public const DELETE_COMPANY = 'Company: Delete';

    // Shift
    public const VIEW_SHIFTS = 'Shift: Index';
    public const CREATE_SHIFT = 'Shift: Create';
    public const VIEW_SHIFT = 'Shift: Show';
    public const EDIT_SHIFT = 'Shift: Edit';
    public const DELETE_SHIFT = 'Shift: Delete';

    // ShiftGroup
    public const VIEW_SHIFT_GROUPS = 'Shift Group: Index';
    public const VIEW_SHIFT_GROUP = 'Shift Group: Show';
    public const EDIT_SHIFT_GROUP = 'Shift Group: Edit';

    // Load
    public const VIEW_LOADS = 'Load: Index';
    public const CREATE_LOAD = 'Load: Create';
    public const VIEW_LOAD = 'Load: Show';
    public const EDIT_LOAD = 'Load: Edit';
    public const DELETE_LOAD = 'Load: Delete';

    /**
     * @return array
     */
    public static function getAll(): array
    {
        return [
            // User
            self::VIEW_USERS,
            self::CREATE_USER,
            self::VIEW_USER,
            self::EDIT_USER,
            self::DELETE_USER,

            // Permission
            self::VIEW_PERMISSIONS,
            self::VIEW_PERMISSION,

            // Role
            self::VIEW_ROLES,
            self::CREATE_ROLE,
            self::VIEW_ROLE,
            self::EDIT_ROLE,
            self::DELETE_ROLE,

            // Company
            self::VIEW_COMPANIES,
            self::CREATE_COMPANY,
            self::VIEW_COMPANY,
            self::EDIT_COMPANY,
            self::DELETE_COMPANY,

            // Shift
            self::VIEW_SHIFTS,
            self::CREATE_SHIFT,
            self::VIEW_SHIFT,
            self::EDIT_SHIFT,
            self::DELETE_SHIFT,

            // ShiftGroup
            self::VIEW_SHIFT_GROUPS,
            self::VIEW_SHIFT_GROUP,
            self::EDIT_SHIFT_GROUP,

            // Load
            self::VIEW_LOADS,
            self::CREATE_LOAD,
            self::VIEW_LOAD,
            self::EDIT_LOAD,
            self::DELETE_LOAD,
        ];
    }
}
