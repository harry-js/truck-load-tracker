<?php

namespace App\Imports;

use App\Company;
use App\Shift;
use App\ShiftGroup;
use App\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ShiftsImport implements ToCollection, WithHeadingRow
{
    /**
     * @var Shift
     */
    private $shift;

    /**
     * @var ShiftGroup
     */
    private $shiftGroup;

    /**
     * @inheritDoc
     */
    public function collection(Collection $rows)
    {
        $company = Company::query()
            ->findOrFail(1);
        $user = User::query()
            ->findOrFail(2);

        $rows->each(function (Collection $row) use ($company, $user) {
            print_r("Importing row: " . $row->implode(', ') . "\n");

            // Create shift group
            if ($shiftGroupId = $row->get('group')) {
                $shiftGroup = ShiftGroup::query()->find($shiftGroupId);

                if (!$shiftGroup) {
                    $shiftGroup = new ShiftGroup();
                    $shiftGroup->id = $shiftGroupId;
                    $shiftGroup->company()->associate($company);
                    $shiftGroup->user()->associate($user);

                    if ($shiftGroupPaidOn = $row->get('paid_on')) { // is paid?
                        $shiftGroup->paid = true;
                        $shiftGroup->paid_on = $shiftGroupPaidOn;
                    }

                    $shiftGroup->save();
                }

                $this->setShiftGroup($shiftGroup);
            }

            // Create shift
            if ($date = $row->get('date')) {
                /** @var Shift $shift */
                $shift = $this->shiftGroup
                    ->shifts()
                    ->create([
                        'date' => $date,
                        'truck' => $row->get('truck'),
                        'pay' => $row->get('pay'),
                    ]);

                $this->setShift($shift);
                $this->createLoad($row, $shift);
            } else { // create load
                $this->createLoad($row, $this->shift);
            }
        });
    }

    /**
     * @param Collection $row
     * @param Shift $shift
     */
    private function createLoad(Collection $row, Shift $shift)
    {
        $shift->loads()
            ->create([
                'trailer' => $row->get('trailer'),
                'from' => strtolower($row->get('from')),
                'to' => strtolower($row->get('to')),
                'pro' => $row->get('pro') ?: 0,
            ]);
    }

    /**
     * @param Shift $shift
     */
    private function setShift(Shift $shift): void
    {
        $this->shift = $shift;
    }

    /**
     * @param ShiftGroup $shiftGroup
     */
    private function setShiftGroup(ShiftGroup $shiftGroup): void
    {
        $this->shiftGroup = $shiftGroup;
    }
}
