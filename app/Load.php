<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property Shift $shift
 * @property bool $bobtail
 * @see Load::getBobtailAttribute()
 * @property string $bobtailFormatted
 * @see Load::getBobtailFormattedAttribute()
 */
class Load extends MyModel
{
    /**
     * @inheritDoc
     */
    protected $fillable = ['trailer', 'from', 'to', 'pro'];

    /**
     * @var string
     */
    public const ICON = 'fas fa-truck-loading';

    /**
     * @return BelongsTo
     */
    public function shift(): BelongsTo
    {
        return $this->belongsTo(Shift::class);
    }

    /**
     * @inheritDoc
     */
    protected function getTitleText(): string
    {
        return "Load #{$this->id}";
    }

    /**
     * @inheritDoc
     */
    protected function getTitleLink(): string
    {
        return link_to_route('shifts.show', $this->getTitleText(), $this);
    }

    /**
     * @return bool
     */
    public function bobtail(): bool
    {
        return is_null($this->trailer);
    }

    /**
     * @return string
     */
    public function getBobtailAttribute()
    {
        return $this->exists && $this->bobtail();
    }

    /**
     * @return string
     */
    public function getBobtailFormattedAttribute()
    {
        $iconClasses = $this->bobtail() ? 'fas fa-check text-success' : 'fas fa-times';

        return "<i class='{$iconClasses}'></i>";
    }
}
