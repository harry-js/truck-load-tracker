<?php

namespace App\Traits;

use App\User;

trait Title
{
    /**
     * @param bool $link
     * @return string
     */
    public function getTitle($link = false): string
    {
        return ($link && $this->hasAccess()) ?
            $this->getTitleLink() :
            $this->getTitleText();
    }

    /**
     * @return bool
     */
    protected function hasAccess(): bool
    {
        return User::signedIn()->can('view', $this);
    }

    /**
     * Get text for title.
     *
     * @return string
     */
    abstract protected function getTitleText(): string;

    /**
     * Get link for title.
     *
     * @return string
     */
    abstract protected function getTitleLink(): string;
}
