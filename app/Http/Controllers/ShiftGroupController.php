<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShiftGroupUpdateRequest;
use App\ShiftGroup;
use App\Transformers\ShiftGroupShiftTransformer;
use App\Transformers\ShiftGroupTransformer;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Yajra\DataTables\Facades\DataTables;

class ShiftGroupController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('shiftGroups.index');
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function dataTableData()
    {
        $with = [
            'company',
            'user',
            'firstShift',
            'lastShift',
        ];
        $query = ShiftGroup::query()
            ->with($with)
            ->select([
                'shift_groups.*',
            ]);

        return Datatables::eloquent($query)
            ->setTransformer(new ShiftGroupTransformer())
            ->makeHidden($with)
            ->make();
    }

    /**
     * @param ShiftGroup $shiftGroup
     * @return Factory|View
     */
    public function show(ShiftGroup $shiftGroup)
    {
        $shiftGroup->load([
            'company',
            'user',
        ]);

        return view('shiftGroups.show', compact('shiftGroup'));
    }

    /**
     * @param ShiftGroup $shiftGroup
     * @return mixed
     */
    public function shiftDataTableData(ShiftGroup $shiftGroup)
    {
        $query = $shiftGroup->shifts()
            ->select([
                'shifts.*',
            ]);

        return Datatables::eloquent($query)
            ->setTransformer(new ShiftGroupShiftTransformer())
            ->make();
    }

    /**
     * @param ShiftGroup $shiftGroup
     * @return Factory|View
     */
    public function edit(ShiftGroup $shiftGroup)
    {
        return view('shiftGroups.edit', compact('shiftGroup'));
    }

    /**
     * @param ShiftGroupUpdateRequest $request
     * @param ShiftGroup $shiftGroup
     * @return RedirectResponse
     */
    public function update(ShiftGroupUpdateRequest $request, ShiftGroup $shiftGroup)
    {
        try {
            $shiftGroup->paid = $request->has('paid');
            $shiftGroup->paid_on = $shiftGroup->paid ?
                $request->input('paid_on') :
                null;
            $shiftGroup->save();
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }

        return redirect()->route('shiftGroups.show', $shiftGroup);
    }
}
