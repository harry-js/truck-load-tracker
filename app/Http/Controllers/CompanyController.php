<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests\CompanyStoreRequest;
use App\Http\Requests\CompanyUpdateRequest;
use App\Shift;
use App\ShiftGroup;
use App\Transformers\CompanyShiftTransformer;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Yajra\DataTables\Facades\DataTables;

class CompanyController extends Controller
{
    /**
     * @return Factory|View
     */
    function index()
    {
        return view('companies.index');
    }

    /**
     * @return mixed
     * @throws Exception
     */
    function dataTableData()
    {
        $query = Company::query()
            ->select([
                'id',
                'name',
                'created_at',
                'updated_at'
            ]);

        return Datatables::of($query)
            ->editColumn('created_at', function (Company $company) {
                return $company->created_at->toDayDateTimeString();
            })
            ->editColumn('updated_at', function (Company $company) {
                return $company->updated_at->toDayDateTimeString();
            })
            ->addColumn('actions', function (Company $company) {
                return view('companies.partials.actions', compact('company'));
            })
            ->rawColumns(['actions'])
            ->make();
    }

    /**
     * @return Factory|View
     */
    function create()
    {
        return view('companies.create');
    }

    /**
     * @param CompanyStoreRequest $request
     * @return RedirectResponse
     */
    function store(CompanyStoreRequest $request)
    {
        try {
            $company = new Company($request->all());
            $company->save();

            return redirect()->route('companies.show', $company);
        } catch (Exception $exception) {
            Log::error($exception->getMessage());

            return back();
        }
    }

    /**
     * @param Company $company
     * @return Factory|View
     */
    function show(Company $company)
    {
        return view('companies.show', compact('company'));
    }

    /**
     * @param Company $company
     * @return mixed
     * @throws Exception
     */
    public function shiftDataTableData(Company $company)
    {
        $with = [
            'company',
            'user',
            'shifts',
            'shifts.group',
            'shifts.group.user',
        ];
        $shifts = $company->shiftGroups()
            ->with($with)
            ->get()
            ->transform(function (ShiftGroup $shiftGroup) {
                return $shiftGroup->shifts;
            })
            ->flatten();

        return Datatables::of($shifts)
            ->setTransformer(new CompanyShiftTransformer())
            ->makeHidden($with)
            ->make();
    }

    /**
     * @param Company $company
     * @return Factory|View
     */
    function edit(Company $company)
    {
        return view('companies.edit', compact('company'));
    }

    /**
     * @param CompanyUpdateRequest $request
     * @param Company $company
     * @return RedirectResponse
     */
    function update(CompanyUpdateRequest $request, Company $company)
    {
        try {
            $company->update($request->all());
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }

        return redirect()->route('companies.show', $company);
    }

    /**
     * @param Company $company
     * @return JsonResponse
     */
    function destroy(Company $company)
    {
        try {
            $company->delete();
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }

        return response()->json(['success' => true]);
    }
}
