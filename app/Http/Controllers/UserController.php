<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequestShiftChartData;
use App\Shift;
use App\ShiftGroup;
use App\Http\Controllers\Crud\UserCrudController;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use Illuminate\View\View;
use Yajra\DataTables\Facades\DataTables;

class UserController extends UserCrudController
{
    /**
     * @return Factory|View
     */
    function index()
    {
        return view('users.index');
    }

    /**
     * @return mixed
     * @throws Exception
     */
    function dataTableData()
    {
        $query = User::query()
            ->select([
                'id',
                'name',
                'email',
                'created_at',
                'updated_at'
            ]);

        return Datatables::of($query)
            ->editColumn('created_at', function (User $user) {
                return $user->created_at->toDayDateTimeString();
            })
            ->editColumn('updated_at', function (User $user) {
                return $user->updated_at->toDayDateTimeString();
            })
            ->addColumn('actions', function (User $user) {
                return view('users.partials.actions', compact('user'));
            })
            ->rawColumns(['actions'])
            ->make();
    }

    /**
     * @return Factory|View
     */
    function create()
    {
        return view('users.create');
    }

    /**
     * @param User $user
     * @return Factory|View
     */
    function show(User $user)
    {
        $yearlyTotalPaid = $user->shifts
            ->groupBy(function (Shift $shift) {
                return $shift->date->year;
            })
            ->transform(function (Collection $shifts) {
                return ShiftGroup::getTotalPaidForShifts($shifts);
            })
            ->toArray();

        return view('users.show', compact('user', 'yearlyTotalPaid'));
    }

    /**
     * @param UserRequestShiftChartData $request
     * @return Collection
     */
    private function defaultShiftChartData(UserRequestShiftChartData $request): Collection
    {
        $data = [
            'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
            'November', 'December'
        ];

        if ($request->input('year') == date('Y')) { // is current year? if yes, show up till current month
            $currentMonthIndex = array_search(date('F'), $data); // index of current month in $data array
            $data = array_slice($data, 0, ($currentMonthIndex + 1)); // grab till current month
        }

        return collect($data)
            ->mapWithKeys(function (string $monthName) {
                return [$monthName => 0]; // set default count to 0
            });
    }

    /**
     * @param UserRequestShiftChartData $request
     * @param User $user
     * @return JsonResponse
     */
    public function shiftChartData(UserRequestShiftChartData $request, User $user)
    {
        $shifts = $user
            ->shifts
            ->filter(function (Shift $shift) use ($request) {
                return ($shift->date->year == $request->input('year')); // filter based on input year
            });

        if ($shifts->isNotEmpty()) {
            $data = $this->defaultShiftChartData($request);

            $shifts
                ->groupBy(function (Shift $shift) {
                    return $shift->date->format('m');
                })
                ->sortKeys()
                ->each(function (Collection $shifts, int $monthNumber) use ($data) {
                    $monthName = Carbon::createFromFormat('m', $monthNumber)
                        ->monthName;

                    $data->put($monthName, $shifts->count());
                });
        }

        return response()->json($data ?? []);
    }

    /**
     * @param User $user
     * @return Factory|View
     */
    function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }
}
