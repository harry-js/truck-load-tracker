<?php

namespace App\Http\Controllers;

use App\Shift;
use App\Http\Requests\ShiftStoreRequest;
use App\Http\Requests\ShiftUpdateRequest;
use App\ShiftGroup;
use App\Transformers\ShiftLoadTransformer;
use App\Transformers\ShiftTransformer;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Yajra\DataTables\Facades\DataTables;

class ShiftController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('shifts.index');
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function dataTableData()
    {
        $with = [
            'group',
            'group.company',
            'group.user',
        ];
        $query = Shift::query()
            ->with($with)
            ->select([
                'shifts.id',
                'shifts.shift_group_id',
                'shifts.date',
                'shifts.pay',
                'shifts.hst',
                'shifts.total_paid',
            ]);

        return Datatables::eloquent($query)
            ->setTransformer(new ShiftTransformer())
            ->makeHidden($with)
            ->make();
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function create(Request $request)
    {
        return view('shifts.create');
    }

    /**
     * @param ShiftStoreRequest $request
     * @return RedirectResponse
     */
    public function store(ShiftStoreRequest $request)
    {
        try {
            $shift = new Shift();

            DB::transaction(function () use ($shift, $request) {
                // Create group
                $group = ShiftGroup::lastOrCreateUnpaid($request->input('company_id'), $request->input('user_id'));

                // Create shift
                $shift->fill($request->all());
                $shift->group()->associate($group);
                $shift->save();
            });

            return redirect()->route('shifts.show', $shift);
        } catch (Exception $exception) {
            Log::error($exception->getMessage());

            return back();
        }
    }

    /**
     * @param Shift $shift
     * @return Factory|View
     */
    public function show(Shift $shift)
    {
        $this->loadRelations($shift);

        return view('shifts.show', compact('shift'));
    }

    /**
     * @param Shift $shift
     */
    private function loadRelations(Shift $shift)
    {
        $shift->load([
            'group',
            'group.company',
            'group.user',
        ]);
    }

    /**
     * @param Shift $shift
     * @return mixed
     */
    public function loadDataTableData(Shift $shift)
    {
        $query = $shift->loads()
            ->select([
                'loads.*',
            ]);

        return Datatables::eloquent($query)
            ->setTransformer(new ShiftLoadTransformer())
            ->make();
    }

    /**
     * @param Shift $shift
     * @return Factory|View
     */
    public function edit(Shift $shift)
    {
        $this->loadRelations($shift);

        return view('shifts.edit', compact('shift'));
    }

    /**
     * @param ShiftUpdateRequest $request
     * @param Shift $shift
     * @return RedirectResponse
     */
    public function update(ShiftUpdateRequest $request, Shift $shift)
    {
        try {
            $shift->fill($request->all());
            $shift->save();
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }

        return redirect()->route('shifts.show', $shift);
    }

    /**
     * @param Shift $shift
     * @return JsonResponse
     */
    public function destroy(Shift $shift)
    {
        try {
            $shift->delete();
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }

        return response()->json(['success' => true]);
    }
}
