<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Spatie\Permission\Models\Permission;
use Yajra\DataTables\Facades\DataTables;

class PermissionController extends Controller
{
    /**
     * @return Factory|View
     */
    function index()
    {
        return view('permissions.index');
    }

    /**
     * @return mixed
     * @throws Exception
     */
    function dataTableData()
    {
        $query = Permission::query()
            ->select([
                'id',
                'name',
            ]);

        return Datatables::of($query)
            ->addColumn('actions', function (Permission $permission) {
                return view('permissions.partials.actions', compact('permission'));
            })
            ->rawColumns(['actions'])
            ->make();
    }

    /**
     * @param Permission $permission
     * @return Factory|View
     */
    function show(Permission $permission)
    {
        return view('permissions.show', compact('permission'));
    }
}
