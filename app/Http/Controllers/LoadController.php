<?php

namespace App\Http\Controllers;

use App\Load;
use App\Http\Requests\LoadStoreRequest;
use App\Http\Requests\LoadUpdateRequest;
use App\Shift;
use App\Transformers\LoadTransformer;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Yajra\DataTables\Facades\DataTables;

class LoadController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('loads.index');
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function dataTableData()
    {
        $with = [
            'shift',
            'shift.group',
            'shift.group.company',
            'shift.group.user',
        ];
        $query = Load::query()
            ->with($with)
            ->select([
                'loads.*',
            ]);

        return Datatables::eloquent($query)
            ->setTransformer(new LoadTransformer())
            ->makeHidden($with)
            ->make();
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function create(Request $request)
    {
        $shifts = Shift::query()
            ->with([
                'group',
                'group.company',
                'group.user',
            ])
            ->get()
            ->mapWithKeys(function (Shift $shift) {
                return [$shift->id => $shift->getTitle()];
            })
            ->toArray();
        $shifts = [null => ''] + $shifts;

        return view('loads.create', compact('shifts'));
    }

    /**
     * @param LoadStoreRequest $request
     * @return RedirectResponse
     */
    public function store(LoadStoreRequest $request)
    {
        try {
            $load = new Load($request->all());
            $load->shift()->associate($request->input('shift_id'));
            $load->save();

            return redirect()->route('loads.show', $load);
        } catch (Exception $exception) {
            Log::error($exception->getMessage());

            return back();
        }
    }

    /**
     * @param Load $load
     * @return Factory|View
     */
    public function show(Load $load)
    {
        $this->loadRelations($load);

        return view('loads.show', compact('load'));
    }

    /**
     * @param Load $load
     */
    private function loadRelations(Load $load)
    {
        $load->load([
            'shift',
            'shift.group',
            'shift.group.company',
            'shift.group.user',
        ]);
    }

    /**
     * @param Load $load
     * @return Factory|View
     */
    public function edit(Load $load)
    {
        $this->loadRelations($load);

        return view('loads.edit', compact('load'));
    }

    /**
     * @param LoadUpdateRequest $request
     * @param Load $load
     * @return RedirectResponse
     */
    public function update(LoadUpdateRequest $request, Load $load)
    {
        try {
            $load->fill($request->all());
            if (!empty($request->input('bobtail'))) {
                $load->trailer = null;
            }
            $load->save();
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }

        return redirect()->route('loads.show', $load);
    }

    /**
     * @param Load $load
     * @return JsonResponse
     */
    public function destroy(Load $load)
    {
        try {
            $load->delete();
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }

        return response()->json(['success' => true]);
    }
}
