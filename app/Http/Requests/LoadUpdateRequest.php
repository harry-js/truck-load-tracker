<?php

namespace App\Http\Requests;

class LoadUpdateRequest extends LoadStoreRequest
{
    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = parent::rules();

        unset($rules['shift_id']);

        return $rules;
    }
}
