<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class RoleRequestUpdate extends RoleRequestStore
{
    /**
     * @inheritDoc
     */
    public function rules()
    {
        /** @var Role $role */
        $role = $this->route('role');
        $rules = parent::rules();
        $rules['name'] = [
            'required',
            'string',
            'max:255',
            Rule::unique('roles', 'name')->ignore($role),
        ];

        return $rules;
    }
}
