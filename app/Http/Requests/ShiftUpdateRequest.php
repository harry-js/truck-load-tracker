<?php

namespace App\Http\Requests;

class ShiftUpdateRequest extends ShiftStoreRequest
{
    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = parent::rules();

        unset($rules['company_id'], $rules['user_id']);

        return $rules;
    }
}
