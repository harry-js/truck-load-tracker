<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoadStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shift_id' => [
                'required',
                'exists:shifts,id'
            ],
            'bobtail' => [
                'required',
                'boolean'
            ],
            'trailer' => [
                'nullable',
                'required_if:bobtail,0',
                'integer',
            ],
            'from' => [
                'required',
            ],
            'to' => [
                'required',
            ],
            'pro' => [
                'required',
                'integer',
                'digits:6',
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function messages()
    {
        return [
            'trailer.required_if' => 'The trailer field is required.'
        ];
    }
}
