<?php

namespace App\Http\Requests;

use App\Company;
use Illuminate\Validation\Rule;

class CompanyUpdateRequest extends CompanyStoreRequest
{
    /**
     * @inheritDoc
     */
    public function rules()
    {
        /** @var Company $company */
        $company = $this->route('company');
        $rules = parent::rules();
        $rules['name'] = [
            'required',
            'string',
            'max:255',
            Rule::unique('companies')->ignore($company),
        ];

        return $rules;
    }
}
