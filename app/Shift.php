<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property Carbon $date
 * @property ShiftGroup $group
 * @property string|null $total_paid_formatted
 * @see Shift::getTotalPaidFormattedAttribute()
 * @property string $date_formatted
 * @see Shift::getDateFormattedAttribute()
 * @property Collection $loads
 */
class Shift extends MyModel
{
    /**
     * @inheritDoc
     */
    protected $fillable = ['date', 'truck', 'pay'];

    /**
     * @inheritDoc
     */
    protected $dates = ['date'];

    /**
     * @var string
     */
    public const ICON = 'fas fa-briefcase';

    /**
     * @return BelongsTo
     */
    public function group(): BelongsTo
    {
        return $this->belongsTo(ShiftGroup::class, 'shift_group_id');
    }

    /**
     * @return HasMany
     */
    public function loads(): HasMany
    {
        return $this->hasMany(Load::class);
    }

    /**
     * Get shifts's date_formatted attribute.
     *
     * @return string
     */
    public function getDateFormattedAttribute(): string
    {
        return $this->date->toFormattedDateString();
    }

    /**
     * Get shifts's total_paid_formatted attribute.
     *
     * @return string|null
     */
    public function getTotalPaidFormattedAttribute()
    {
        return self::getFormattedTotalPaid($this->pay, $this->hst, $this->total_paid);
    }

    /**
     * @param $pay
     * @param $hst
     * @param $totalPaid
     * @return string|null
     */
    public static function getFormattedTotalPaid($pay, $hst, $totalPaid): ?string
    {
        if ($pay && $hst && $totalPaid) {
            $title = "Pay = {$pay}\nHST = {$hst}";
            $formattedTotalPaid = "<span title='{$title}'>{$totalPaid}</span>";
        } else {
            $formattedTotalPaid = null;
        }

        return $formattedTotalPaid;
    }

    /**
     * Set hst and total_paid attributes.
     *
     * @return void
     */
    public function setPayAttributes(): void
    {
        if ($this->pay) {
            $this->hst = ($this->pay * config('app.hstPercentage') / 100);
            $this->total_paid = ($this->pay + $this->hst);
        } else {
            $this->hst =
            $this->total_paid = null;
        }
    }

    /**
     * @inheritDoc
     */
    protected function getTitleText(): string
    {
        $userName = $this->group->user->getTitle();
        $companyName = $this->group->company->getTitle();
        $date = $this->date_formatted;

        return "{$userName} @ {$companyName} on {$date}";
    }

    /**
     * @inheritDoc
     */
    protected function getTitleLink(): string
    {
        return link_to_route('shifts.show', $this->getTitleText(), $this);
    }

    /**
     * @return bool
     */
    public function isPaid(): bool
    {
        return $this->group->isPaid();
    }

    /**
     * @return bool
     */
    public function isUnpaid(): bool
    {
        return ! $this->isPaid();
    }
}
