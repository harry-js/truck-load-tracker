<?php

namespace App\Observers;

use App\Shift;

class ShiftObserver
{
    /**
     * Handle the shift "creating" event.
     *
     * @param Shift $shift
     * @return void
     */
    public function creating(Shift $shift)
    {
        $shift->setPayAttributes();
    }

    /**
     * Handle the shift "updating" event.
     *
     * @param Shift $shift
     * @return void
     */
    public function updating(Shift $shift)
    {
        if ($shift->isDirty('pay')) {
            $shift->setPayAttributes();
        }
    }
}
