<?php

namespace App\Providers;

use App\Observers\ShiftObserver;
use App\Shift;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Shift::observe(ShiftObserver::class);
    }
}
