<?php

namespace App\Providers;

use App\Company;
use App\Load;
use App\Policies\CompanyPolicy;
use App\Policies\LoadPolicy;
use App\Policies\PermissionPolicy;
use App\Policies\RolePolicy;
use App\Policies\ShiftGroupPolicy;
use App\Policies\ShiftPolicy;
use App\Policies\UserPolicy;
use App\Shift;
use App\ShiftGroup;
use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
        Permission::class => PermissionPolicy::class,
        Role::class => RolePolicy::class,
        Company::class => CompanyPolicy::class,
        Shift::class => ShiftPolicy::class,
        ShiftGroup::class => ShiftGroupPolicy::class,
        Load::class => LoadPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
