<?php

namespace App\Console\Commands;

use App\Imports\ShiftsImport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class ImportRecords extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importRecords';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import records from csv file';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->info('Importing...');

        Excel::import(new ShiftsImport, 'import.csv');
    }
}
