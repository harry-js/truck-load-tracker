<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * @property Collection $shifts
 * @property Carbon $paid_on
 * @property Company $company
 * @property User $user
 * @property string $pay_status
 * @see ShiftGroup::getPayStatusAttribute()
 * @property string $paid_on_formatted
 * @see ShiftGroup::getPaidOnFormattedAttribute()
 * @property Shift $firstShift
 * @see ShiftGroup::firstShift()
 * @property Shift $lastShift
 * @see ShiftGroup::lastShift()
 * @property string $first_shift_date
 * @see ShiftGroup::getFirstShiftDateAttribute()
 * @property string $last_shift_date
 * @see ShiftGroup::getLastShiftDateAttribute()
 * @property string $total_paid
 * @see ShiftGroup::getTotalPaidAttribute()
 * @method static Builder|self paid()
 * @see ShiftGroup::scopePaid()
 * @method static Builder|self notPaid()
 * @see ShiftGroup::scopeNotPaid()
 */
class ShiftGroup extends MyModel
{
    /**
     * @inheritDoc
     */
    protected $casts = [
        'paid' => 'boolean',
    ];

    /**
     * @inheritDoc
     */
    protected $dates = ['paid_on'];

    /**
     * @var string
     */
    public const ICON = 'fas fa-briefcase';

    /**
     * @return BelongsTo
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return HasMany
     */
    public function shifts(): HasMany
    {
        return $this->hasMany(Shift::class);
    }

    /**
     * @return HasOne
     */
    public function firstShift(): HasOne
    {
        return $this->hasOne(Shift::class)
            ->orderBy('date');
    }

    /**
     * @return HasOne
     */
    public function lastShift(): HasOne
    {
        return $this->hasOne(Shift::class)
            ->orderBy('date', 'desc');
    }

    /**
     * @return bool
     */
    public function isPaid(): bool
    {
        return $this->paid;
    }

    /**
     * @return bool
     */
    public function isUnpaid()
    {
        return !$this->isPaid();
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopePaid(Builder $builder)
    {
        return $builder->where('paid', '=', true);
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeNotPaid(Builder $builder)
    {
        return $builder->where('paid', '=', false);
    }

    /**
     * Get the last unpaid group or create one.
     *
     * @param $companyId
     * @param $userId
     * @return Model|self
     */
    public static function lastOrCreateUnpaid($companyId, $userId): self
    {
        // Grab existing
        $group = self::notPaid()
            ->where('company_id', $companyId)
            ->where('user_id', $userId)
            ->orderBy('id', 'desc')
            ->first();

        if (! $group) {
            // Create new
            $group = new self;
            $group->company()->associate($companyId);
            $group->user()->associate($userId);
            $group->save();
        }

        return $group;
    }

    /**
     * Get 'pay_status' attribute.
     *
     * @return string
     */
    public function getPayStatusAttribute()
    {
        return $this->isPaid() ?
            "
            <span title='" . ($this->paid_on_formatted ? "Paid on {$this->paid_on_formatted}" : null) . "'>
                Paid
                <i class='fas fa-check text-success'></i>
            </span>
            " :
            'Unpaid';
    }

    /**
     * @return string|null
     */
    public function getPaidOnFormattedAttribute()
    {
        return ($this->isPaid() && $this->paid_on) ?
            $this->paid_on->toFormattedDateString() :
            null;
    }

    /**
     * @inheritDoc
     */
    protected function getTitleText(): string
    {
        return "Group #{$this->id}";
    }

    /**
     * @inheritDoc
     */
    protected function getTitleLink(): string
    {
        return link_to_route('shiftGroups.show', $this->getTitleText(), $this);
    }

    /**
     * Get 'first_shift_date' attribute.
     *
     * @return string
     */
    public function getFirstShiftDateAttribute()
    {
        return $this->firstShift->date_formatted;
    }

    /**
     * Get 'last_shift_date' attribute.
     *
     * @return string
     */
    public function getLastShiftDateAttribute()
    {
        return $this->lastShift->date_formatted;
    }

    /**
     * Get 'total_paid' attribute.
     *
     * @return string|null
     */
    public function getTotalPaidAttribute()
    {
        if (! $this->relationLoaded('shifts')) {
            $this->load('shifts');
        }

        return self::getTotalPaidForShifts($this->shifts);
    }

    /**
     * @param Collection $shifts
     * @return string|null
     */
    public static function getTotalPaidForShifts(Collection $shifts): ?string
    {
        $totalPay = $shifts->sum('pay');
        $totalHst = $shifts->sum('hst');
        $totalPaid = $shifts->sum('total_paid');

        return Shift::getFormattedTotalPaid($totalPay, $totalHst, $totalPaid);
    }
}
