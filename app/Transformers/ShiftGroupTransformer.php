<?php

namespace App\Transformers;

use App\ShiftGroup;
use League\Fractal\TransformerAbstract;
use Throwable;

class ShiftGroupTransformer extends TransformerAbstract
{
    /**
     * @param ShiftGroup $shiftGroup
     * @return array
     * @throws Throwable
     */
    public function transform(ShiftGroup $shiftGroup)
    {
        $row = [
            'company_name' => $shiftGroup->company->getTitle(true),
            'user_name' => $shiftGroup->user->getTitle(true),
            'pay_status' => $shiftGroup->pay_status,
            'first_shift_date' => $shiftGroup->first_shift_date,
            'last_shift_date' => $shiftGroup->last_shift_date,
            'actions' => view('shiftGroups.partials.actions', compact('shiftGroup'))->render(),
        ];

        return $row + $shiftGroup->toArray();
    }
}
