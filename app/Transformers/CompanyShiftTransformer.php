<?php

namespace App\Transformers;

use App\Shift;
use League\Fractal\TransformerAbstract;

class CompanyShiftTransformer extends TransformerAbstract
{
    /**
     * @param Shift $shift
     * @return array
     * @throws \Throwable
     */
    public function transform(Shift $shift)
    {
        $row = [
            'date' => $shift->date_formatted,
            'user_name' => $shift->group->user->getTitle(true),
            'pay_status' => $shift->group->pay_status,
            'total_paid' => $shift->total_paid_formatted,
            'created_at' => $shift->created_at->toDayDateTimeString(),
            'updated_at' => $shift->updated_at->toDayDateTimeString(),
            'actions' => view('shifts.partials.actions', compact('shift'))->render(),
        ];

        return $row + $shift->toArray();
    }
}
