<?php

namespace App\Transformers;

use App\Shift;
use League\Fractal\TransformerAbstract;
use Throwable;

class ShiftTransformer extends TransformerAbstract
{
    /**
     * @param Shift $shift
     * @return array
     * @throws Throwable
     */
    public function transform(Shift $shift)
    {
        $row = [
            'company_name' => $shift->group->company->getTitle(true),
            'user_name' => $shift->group->user->getTitle(true),
            'date' => $shift->date_formatted,
            'total_paid' => $shift->total_paid_formatted,
            'pay_status' => $shift->group->pay_status,
            'actions' => view('shifts.partials.actions', compact('shift'))->render(),
        ];

        return $row + $shift->toArray();
    }
}
