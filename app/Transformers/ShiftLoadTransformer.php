<?php

namespace App\Transformers;

use App\Load;
use League\Fractal\TransformerAbstract;

class ShiftLoadTransformer extends TransformerAbstract
{
    /**
     * @param Load $load
     * @return array
     * @throws \Throwable
     */
    public function transform(Load $load)
    {
        $row = [
            'created_at' => $load->created_at->toDayDateTimeString(),
            'updated_at' => $load->updated_at->toDayDateTimeString(),
            'actions' => view('loads.partials.actions', compact('load'))->render(),
        ];

        return $row + $load->toArray();
    }
}
