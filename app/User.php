<?php

namespace App;

use App\Entities\Role as RoleEntity;
use App\Traits\Title as TitleTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

/**
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Collection $shifts
 * @see User::getShiftsAttribute()
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use TitleTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'email_verified_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @var string
     */
    public const DEFAULT_PASSWORD = 'password';

    /**
     * @var string
     */
    public const SUPPORT_TEAM_EMAIL = 'support@truckloadtracker.test';

    /**
     * Set the user's password.
     *
     * @param  string  $value
     * @return void
     */
    public function setPasswordAttribute($value): void
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Is the user super admin?
     *
     * @return bool
     */
    public function isSuperAdmin(): bool
    {
        return $this->hasRole(RoleEntity::SUPER_ADMIN);
    }

    /**
     * @inheritDoc
     */
    protected function getTitleText(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    protected function getTitleLink(): string
    {
        return link_to_route('users.show', $this->getTitleText(), $this);
    }

    /**
     * @return self|\Illuminate\Contracts\Auth\Authenticatable
     */
    public static function signedIn(): self
    {
        return Auth::user();
    }

    /**
     * @return HasMany
     */
    public function shiftGroups(): HasMany
    {
        return $this->hasMany(ShiftGroup::class);
    }

    /**
     * @return Collection
     */
    public function getShiftsAttribute()
    {
        return $this->shiftGroups()
            ->with('shifts')
            ->get()
            ->transform(function (ShiftGroup $shiftGroup) {
                return $shiftGroup->shifts;
            })
            ->flatten();
    }
}
