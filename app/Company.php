<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Company extends MyModel
{
    /**
     * @inheritDoc
     */
    protected $fillable = ['name'];

    /**
     * @return HasMany
     */
    public function shiftGroups(): HasMany
    {
        return $this->hasMany(ShiftGroup::class);
    }

    /**
     * @inheritDoc
     */
    protected function getTitleText(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    protected function getTitleLink(): string
    {
        return link_to_route('companies.show', $this->getTitleText(), $this);
    }
}
