## About Truck Load Tracker
Truck Load Tracker is a web application for keeping record of day-to-day work of truck drivers. Trucking company assigns 
a load (shipment in a trailer) to a driver for its delivery from origin to destination. Each trip is assigned a 
unique number, called "PRO number", by the company. The app consists of administration  system along with other modules 
such as company and shift.

## Features
- Elegant dashboard for various entities
- ACL via permissions
- Graphical representation of shift information

## Concept Overview
- Administration module
    - Entities: User, Permission and Role
    - ACL is controlled via permissions
    - Permissions are seeded using `PermissionSeeder`
    - User with required permission can create any number of roles. Each role can then be assigned one or more permissions
- Company module
    - Entities: Company
- Shift module
    - Entities: Shift Group, Shift, Load
    - A shift is associated to a company and user
    - System will handle grouping of unpaid shifts. This allows user to mark a Shift Group as paid
    - Shift will contain information about truck used by user (or driver) and its total pay
    - Each shift has one or more loads associated with it
    - Load information includes trailer number, from, to and more fields
    
## Screenshots
- Administration module
![Screenshot of administration module](readme/screenshots/admin.gif "Screenshot of administration module")
- Company module
![Screenshot of company module](readme/screenshots/company.gif "Screenshot of company module")
- Shift module
![Screenshot of shift module](readme/screenshots/shift.gif "Screenshot of shift module")

## Todo List
- Notify user on account creation
- Use Laravel Passport for API Authentication
- Add date range to shift chart for a user
