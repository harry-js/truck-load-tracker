<?php

use App\Company;
use App\Shift;
use App\ShiftGroup;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class ShiftSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment('local')) {
            $companies = Company::all();
            $faker = Faker\Factory::create();

            User::all()
                ->each(function (User $user) use ($companies, $faker) {
                    // Create shift group
                    $shiftGroup = new ShiftGroup();
                    $shiftGroup->company()->associate($companies->random());
                    $shiftGroup->user()->associate($user);

                    if (rand(0, 1)) {
                        $shiftGroup->paid = true;
                        $shiftGroup->paid_on = $faker->date();
                    }

                    $shiftGroup->save();

                    $shiftsCount = $faker->numberBetween(1, 20);
                    $i = 0;

                    do {
                        // Create shifts
                        /** @var Shift $shift */
                        $shift = $shiftGroup->shifts()
                            ->create([
                                'date' => $faker->date(),
                                'truck' => $faker->randomNumber(4),
                                'pay' => $faker->randomElement([100, 120, 180, 230]),
                            ]);
                        $i++;

                        // Create loads
                        $j = 0;
                        do {
                            $shift->loads()
                                ->create([
                                    'trailer' => $faker->randomNumber(4),
                                    'from' => $faker->city,
                                    'to' => $faker->city,
                                    'pro' => $faker->randomNumber(6),
                                ]);
                            $j++;
                        } while ($j < 2);
                    } while ($i < $shiftsCount);
                });
        }
    }
}
