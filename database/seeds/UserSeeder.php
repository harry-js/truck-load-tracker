<?php

use App\Entities\Role as RoleEntity;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var User $supportTeamUser */
        $supportTeamUser = User::query()
            ->create([
                'name' => 'Support Team',
                'email' => User::SUPPORT_TEAM_EMAIL,
                'password' => User::DEFAULT_PASSWORD,
                'email_verified_at' => now(),
            ]);
        $supportTeamUser->assignRole(RoleEntity::SUPER_ADMIN);

        if (App::environment('local')) {
            factory(User::class, 20)->create();
        }
    }
}
