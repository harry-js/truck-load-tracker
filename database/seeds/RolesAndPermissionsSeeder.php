<?php

use App\Entities\Permission as PermissionEntity;
use App\Entities\Role as RoleEntity;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $this->syncPermissions();
        $this->syncRoles();
    }

    /**
     * Delete and create new permissions based on permission entity.
     *
     * @return void
     */
    private function syncPermissions(): void
    {
        $existingPermissions = Permission::all()
            ->transform(function (Permission $permission) {
                return $permission->name;
            })
            ->toArray();
        $permissions = PermissionEntity::getAll();

        // Delete if not present.
        $deletePermissions = array_diff($existingPermissions, $permissions);
        Permission::query()
            ->whereIn('name', $deletePermissions)
            ->delete();

        // Create new.
        $createPermissions = array_diff($permissions, $existingPermissions);
        foreach ($createPermissions as $permissionName) {
            Permission::create(['name' => $permissionName]);
        }
    }

    /**
     * Delete and create new roles based on role entity.
     *
     * @return void
     */
    private function syncRoles(): void
    {
        $existingRoles = Role::all()
            ->transform(function (Role $role) {
                return $role->name;
            })
            ->toArray();
        $roles = RoleEntity::getAll();

        // Delete if not present.
        $deleteRoles = array_diff($existingRoles, $roles);
        Role::query()
            ->whereIn('name', $deleteRoles)
            ->delete();

        // Create new.
        $createRoles = array_diff($roles, $existingRoles);
        foreach ($createRoles as $roleName) {
            /** @var Role $role */
            $role = Role::create(['name' => $roleName]);
            $role->givePermissionTo(RoleEntity::getPermissions($roleName));
        }

        // Sync permissions.
        Role::query()
            ->whereIn('name', $roles)
            ->each(function (Role $role) {
                $role->syncPermissions(RoleEntity::getPermissions($role->name));
            });
    }
}
