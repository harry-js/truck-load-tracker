<?php

use App\Company;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment('local')) {
            factory(Company::class, 20)->create();
        }
    }
}
