<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loads', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('shift_id');
            $table->foreign('shift_id')
                ->references('id')
                ->on('shifts')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->unsignedMediumInteger('trailer')->nullable();
            $table->string('from');
            $table->string('to');
            $table->unsignedMediumInteger('pro');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loads');
    }
}
