<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shifts', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('shift_group_id');
            $table->foreign('shift_group_id')
                ->references('id')
                ->on('shift_groups')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->date('date');
            $table->unsignedInteger('truck');
            $table->float('pay')
                ->nullable();
            $table->float('hst')
                ->nullable();
            $table->float('total_paid')
                ->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shifts');
    }
}
